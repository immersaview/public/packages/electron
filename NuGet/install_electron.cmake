# creates an electon executable in the given install directory
# install_electron(EXECUTABLE_NAME <executable-name> ICON_FILE <file>
#                  VERSION <version-string> INSTALL_DIR <dir>)
function(install_electron)
    set(OPTION_KEYS)
    set(ONE_VAL_KEYS EXECUTABLE_NAME ICON_FILE VERSION INSTALL_DIR)
    set(MULTI_VAL_KEYS)
    cmake_parse_arguments(INSTALL_ELECTRON "${OPTION_KEYS}" "${ONE_VAL_KEYS}" "${MULTI_VAL_KEYS}" "${ARGN}")

    if (NOT EXISTS "${ELECTRON_PACKAGE_DIR}")
        message(FATAL_ERROR "'\${ELECTRON_PACKAGE_DIR}' is not set to a valid path.")
    endif ()

    # Install files
    install(DIRECTORY "${ELECTRON_PLATFORM_DIR}/release/" DESTINATION "${INSTALL_ELECTRON_INSTALL_DIR}" FILES_MATCHING
        PATTERN "*"
        PATTERN "electron${CMAKE_EXECUTABLE_SUFFIX}" EXCLUDE
        PATTERN "default_app.asar" EXCLUDE
        PATTERN "LICENSE" EXCLUDE
    )
    install(PROGRAMS "${ELECTRON_PLATFORM_DIR}/release/LICENSE" DESTINATION "${INSTALL_ELECTRON_INSTALL_DIR}" RENAME "LICENSE.electron.txt")

    # Install executable
    if (WIN32)
        set(RCEDIT_EXE "${NODE_MODULES}/rcedit/bin/rcedit.exe")
        string(TIMESTAMP CURRENT_YEAR "%Y" UTC)

        # We have to "build" the executable
        add_custom_target(electron-executable ALL
            COMMAND powershell -c "if ( ! (Test-Path '${NODE_MODULES}') ) { \
                                       echo 'NODE_MODULES=${NODE_MODULES} is not set to a valid node modules folder.'; \
                                       exit 1; \
                                   }"
            COMMAND ${CMAKE_COMMAND} -E copy "${ELECTRON_PLATFORM_DIR}/release/electron${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_BINARY_DIR}"
            COMMAND ${CMAKE_COMMAND} -E rename "${CMAKE_CURRENT_BINARY_DIR}/electron${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_BINARY_DIR}/${INSTALL_ELECTRON_EXECUTABLE_NAME}${CMAKE_EXECUTABLE_SUFFIX}"
            COMMAND ${RCEDIT_EXE} ${INSTALL_ELECTRON_EXECUTABLE_NAME}${CMAKE_EXECUTABLE_SUFFIX}
                --set-icon             "${INSTALL_ELECTRON_ICON_FILE}"
                --set-version-string   "LegalCopyright"   "Copyright © ImmersaView Pty Ltd ${CURRENT_YEAR}"
                --set-version-string   "ProductName"      "${INSTALL_ELECTRON_EXECUTABLE_NAME}"
                --set-version-string   "FileDescription"  "${INSTALL_ELECTRON_EXECUTABLE_NAME}"
                --set-product-version  "${INSTALL_ELECTRON_VERSION}"
                --set-file-version     "${INSTALL_ELECTRON_VERSION}"
                --set-version-string   "CompanyName"      "ImmersaView Pty Ltd"
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        )

        install(PROGRAMS "${CMAKE_CURRENT_BINARY_DIR}/${INSTALL_ELECTRON_EXECUTABLE_NAME}${CMAKE_EXECUTABLE_SUFFIX}" DESTINATION "${INSTALL_ELECTRON_INSTALL_DIR}")
    elseif (UNIX)
        install(PROGRAMS "${ELECTRON_PLATFORM_DIR}/release/electron${CMAKE_EXECUTABLE_SUFFIX}" DESTINATION "${INSTALL_ELECTRON_INSTALL_DIR}" RENAME "${INSTALL_ELECTRON_EXECUTABLE_NAME}${CMAKE_EXECUTABLE_SUFFIX}")
    endif ()
endfunction()