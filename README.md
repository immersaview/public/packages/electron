## Download Electron
1. Go to https://www.electronjs.org/releases/stable
1. Download selected binaries. E.g. https://github.com/electron/electron/releases/tag/v7.3.3 
    - electron-v7.3.3-win32-x64.zip
    - electron-v7.3.3-win32-ia32.zip
    - electron-v7.3.3-linux-x64.zip
    - electron-v7.3.3-linux-ia32.zip
1. Create `Electron-7.3.3` folder in root
1. Create `Linux` and `Windows` sub dirs
1. Inside each sub dir create `x86` and `x64` folders
1. Extract each `.zip` file into specified arch folder
1. Delete previous electron version folder. E.g. `Electron-3.0.2`

# Getting latest nodejs libs for Electron

Follow instructions at [nodejs](https://github.com/nodejs/node-gyp) for installing .node-gyp and prerequisites.
Also see [Electron-using-native-modules](https://github.com/electron/electron/blob/master/docs/tutorial/using-native-node-modules.md) for a better understanding.

Run `node-gyp install --target=v1.7.10 --dist-url=https://atom.io/download/electron --devdir=./node` 
replacing target with the appropriate electron version and replacing devdir with a suitable download directory.  If dist 
URL is not valid anymore it will need some research for the correct URL as electron is overriding 
node-gyps defaults which does not link to electrons version.

### Install Node.js header files for the given version
1. Run `node-gyp install --target=v7.3.3 --dist-url=https://atom.io/download/electron --devdir=./node`
1. Rename `node` folder to `node-7.3.3`

# Getting latest nan and Node Addon API headers

Run the commands below to install the headers to the packages location.  If npm is not 
available see node-gyp for installing npm.

### Update `nan` and `node-addon-api` packages
1. Run `npm install nan`
1. Run `npm install node-addon-api`
1. Click on `node_modules` folder and copy both `nan` and `node-addon-api` folders to the root
1. Delete `node_modules` folder.


## Update `electron.nuspec`
1. Update `<version>`
1. Update `<releaseNotes>`
1. Update `<copyright>`
1. Make sure to update all version numbers in `<files>`

## Testing Locally
1. Push your branch up to code.immersaview.com
1. Wait for pipeline to build then download artifacts
1. Copy then extract `.nupkg` to your local code repo. e.g. `C:\immersaview\simvisuals\packages\Imv.External.electron.7.3.3.20200921`
1. Make sure to keep the `.nupkg` file. Do not remove as build script will not work without it.
1. Run cmake and build scripts
1. Run Visual Studio

## A note about `win_delay_load_hook`
On Windows, by default, node-gyp links native modules against node.dll. However, in Electron 4.x and higher, the symbols needed by native modules are exported by electron.exe, 
and there is no node.dll. In order to load native modules on Windows, node-gyp installs a delay-load hook that triggers when the native module is loaded, and redirects the node.dll 
reference to use the loading executable instead of looking for node.dll in the library search path (which would turn up nothing). As such, on Electron 4.x and higher,
'win_delay_load_hook': 'true' is required to load native modules.

See: https://www.electronjs.org/docs/tutorial/using-native-node-modules#a-note-about-win_delay_load_hook

# Example Usage
## Adding NodeJS to a target
```cmake
target_link_libraries(target node)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:node,INTERFACE_INCLUDE_DIRECTORIES>")
```
## Using the Electron executable
The package directory is available using the `ELECTRON_PACKAGE_DIR` variable. For example, the 64-bit Windows Electron executable is located at `${ELECTRON_PACKAGE_DIR}/bin/Electron/Windows/x64/electron.exe`
